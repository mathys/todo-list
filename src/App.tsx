import './App.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";

import {ReadAll} from "./component/readAll";
import {CreateBody} from "./component/create";
import {Read} from "./component/read";
import {UpdateBody} from "./component/update";
import {DeleteBody} from "./component/delete";


function App() {
  return (
    <div className="App container">
      <div className="col-lg-12 text-center">
          <h1>Todo List</h1><br></br>
          <hr></hr>
      </div>
      <Router>
          <Route path="/" exact component={ReadAll}></Route>
          <Route path="/create" component={CreateBody}></Route>
          <Route path="/read" component={Read}></Route>
          <Route path="/update" component={UpdateBody}></Route>
          <Route path="/delete" component={DeleteBody}></Route>
      </Router>
    </div>
    
  );
}


export default App;
