import React, { useCallback, useState } from "react";
import {querryObject} from "../interface/querryObject";
import {Link} from "react-router-dom";
import { Provider, useDispatch } from "react-redux";
import { createAction } from "../store/todoAction";
import store from "../store/store";
export function CreateBody(data: querryObject){
    return (
        <Provider store={store}>
            <CreateItems data={data}/>
        </Provider>
    )
}

function Create(self: any){
    return (
        <div>
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h2>New Todo</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr></hr>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-1">
                    <span>Title :</span>
                </div>
                <div className="col-lg-5">
                    <textarea onChange={self.handleTitle} className='form-control'></textarea>
                </div>
                <div className="col-lg-1">
                    <span>Body :</span>
                </div>
                <div className="col-lg-5">
                    <textarea onChange={self.handleContent} className='form-control'></textarea>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12 text-center">
                    <br></br><Link to='/' onClick={() => self.create(self.result())} className="btn btn-success">apply</Link>
                </div>
            </div>
        </div>
    )
}

function CreateItems(props:{data: any}){
    const dispatch = useDispatch();
    const create = useCallback((todo) => {
        dispatch(createAction(todo))
    }, [dispatch]);
    const [content, setContent] = useState(" ");
    const [title, setTitle] = useState(" ");
    const handleTitle = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setTitle(event.target.value);  
    };
    const handleContent = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setContent(event.target.value);  
    };
    const result = () => {
       return ({title:title, content:content});
    };
    const test = <Create handleTitle={handleTitle} handleContent={handleContent} result={result} create={create}/>;
    return  test; 
}