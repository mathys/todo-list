import React, { useCallback } from "react";
import {querryObject} from "../interface/querryObject";
import {Link} from "react-router-dom";
import { Provider, useDispatch } from "react-redux";
import { deleteAction } from "../store/todoAction";
import store from "../store/store";
export function DeleteBody(data: querryObject){
    return (
        <Provider store={store}>
            <DeleteItems data={data.location.state}/>
        </Provider>
    )
}

function Delete(self: any){
    return (
        <div>
            <div className="row"> 
                <div className="col-lg-12 text-center">
                    <h2>Todo Deleted</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12 text-center">
                    {self.delete(self.data)}
                    <br></br><Link to='/' className="btn btn-primary">back</Link>
                </div>
            </div>
        </div>
    )
}

function DeleteItems(props:{data: any}){
    const dispatch = useDispatch();
    const del = useCallback((todo) => {
        dispatch(deleteAction(todo))
    }, [dispatch]);
    const test = <Delete data={props.data} delete={del}/>;
    return  test; 
}