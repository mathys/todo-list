import React from "react";
import {querryObject} from "../interface/querryObject";
import {Link} from "react-router-dom";
export function Read(data: querryObject){
    return (
        <div>
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h2>{data.location.state.title}</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <p>content: {data.location.state.content}</p>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr></hr>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12 text-center">
                <br></br><Link to='/' className="btn btn-primary">back</Link>
                </div>
            </div>
        </div>
    )
}
