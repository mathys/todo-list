import React from "react";
import { Provider } from 'react-redux'
import store from "../store/store";
import {TodoList} from './todoList';
// import {TodoListStore} from './todoList';
import {Link} from "react-router-dom";
export function ReadAll(){
    return (
        <Provider store={store}>
            <TodoList/>
            <div className="row">
                <div className="col-lg-12 text-center">
                    <Link to="/create" className="btn btn-success">create</Link>
                </div>
            </div>
        </Provider>
    );
}