import { useSelector} from "react-redux";
import {todo} from "../interface/todo";
import {Link} from "react-router-dom";
import {todoSelector} from "../store/todoSelector";
import React from "react";

function TodoItem({todo}: any){
    return(
        <div>
            <div className="row">
                <div className="col-lg-9">
                    <h2>{todo.title}</h2>
                </div>
                <div className="col-lg-1">
                    <Link to={{pathname: '/read', state:{id: todo.id, title: todo.title, content: todo.content}}} className="btn btn-secondary">read</Link>
                </div>
                <div className="col-lg-1">
                    <Link to={{pathname: '/update', state:{id: todo.id, title: todo.title, content: todo.content}}} className="btn btn-primary">update</Link>
                </div>
                <div className="col-lg-1">
                    <Link to={{pathname: '/delete', state:{id: todo.id, title: todo.title, content: todo.content}}} className="btn btn-danger">delete</Link>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr></hr>
                </div>
            </div>
        </div>
    )
}

export function TodoList(){
    const todos: any = useSelector(todoSelector);
    
    return (<ul>
        {todos.map((todo: todo) => <TodoItem todo={todo} key={todo.id}></TodoItem>)}
    </ul>)
}