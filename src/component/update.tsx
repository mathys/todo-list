import React, { useCallback, useState } from "react";
import {querryObject} from "../interface/querryObject";
import {Link} from "react-router-dom";
import { Provider, useDispatch } from "react-redux";
import { toggleTodoAction } from "../store/todoAction";
import store from "../store/store";
export function UpdateBody(data: querryObject){
    return (
        <Provider store={store}>
            <UpdateItems data={data}/>
        </Provider>
    )
}

function Update(self: any){
   
    return (
        <div>
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h1>{self.data.title}</h1>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr></hr>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-1">
                    <span>Title :</span>
                </div>
                <div className="col-lg-5">
                    <textarea onChange={self.handleTitle} className='form-control'></textarea>
                </div>
                <div className="col-lg-1">
                    <span>Body :</span>
                </div>
                <div className="col-lg-5">
                    <textarea onChange={self.handleContent} className='form-control'></textarea>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12 text-center">
                    <br></br><Link to='/' onClick={() => self.update(self.result(self.data.id))} className="btn btn-success">apply</Link>
                </div>
            </div>
        </div>
    )
}

function UpdateItems(props:{data: any}){
    const dispatch = useDispatch();
    const update = useCallback((todo) => {
        dispatch(toggleTodoAction(todo))
    }, [dispatch]);
    const [content, setContent] = useState(" ");
    const [title, setTitle] = useState(" ");
    const handleTitle = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setTitle(event.target.value);  
    };
    const handleContent = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setContent(event.target.value);  
    };
    const result = (id: number) => {
       return ({title:title, content:content, id: id});
    };
    const test = <Update handleTitle={handleTitle} handleContent={handleContent} result={result} update={update} data={props.data.location.state}/>;
    return  test; 
}