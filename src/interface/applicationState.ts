export interface ApplicationState{
    id: number, 
    title: string, 
    content: string 
}