import { createStore, combineReducers} from 'redux';
import { todoReducer} from './todosReducer';




const rootReducer =  createStore(
    combineReducers({
        todos: todoReducer, 
        filter : (state = 10, action) => state
    })
);


export default rootReducer;
