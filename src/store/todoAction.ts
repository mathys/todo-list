export const toggleTodoAction = (todo: any) => ({
    type: 'UPDATE', 
    payload: {...todo}
});
export const createAction = (todo: any) => ({
    type: 'CREATE', 
    payload: {...todo}
})
export const deleteAction = (todo: any) => ({
    type: 'DELETE', 
    payload: {...todo}
})