const initialState = [
    {
        id:1,
        title: "first",
        content: "zatehryjhzeryjeytj"
    },
    {
        id:2,
        title: "second",
        content: "zatehryjhzedfsdfzersfsdfryjeytj"
    }
];
export function todoReducer (state = initialState, action:any){
    switch(action.type){
        case 'CREATE':
            return [...state, {id : state.length+1, ...action.payload}];
        case 'UPDATE':
            return state.map(todo => {
                if (todo.id === action.payload.id){
                    return {...todo,  ...action.payload};
                }else{
                    return todo;
                }
            });

            case 'DELETE':
                return state.filter(todo => todo.id !== action.payload.id);
        default:
            return state;
    }

}